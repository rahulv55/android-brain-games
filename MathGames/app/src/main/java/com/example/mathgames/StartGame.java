package com.example.mathgames;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;

public
class StartGame extends AppCompatActivity {

    int op1, op2,sum,sumOthers;
    TextView tvTimer,tvPoints,tvSum,tvResult;
    Button btn0,btn1,btn2,btn3;
    CountDownTimer countDownTimer;
    long millisUntilFinished;
    int points;
    int numberOfQuestions;
    Random random;
    int[] btnIds;
    int correctAnswerPosition;
    ArrayList<Integer> incorrectAnswers;

    @Override
    public  void onCreate(@Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState  );
        setContentView( R.layout.start_game );
        op1=0;
        op2=0;
        sum=0;
        btn0 = findViewById( R.id.btn0 );
        btn1 = findViewById( R.id.btn1 );
        btn2 = findViewById( R.id.btn2 );
        btn3 = findViewById( R.id.btn3 );
        tvTimer = findViewById( R.id.tvTimer );
        tvPoints = findViewById( R.id.tvPoints);
        tvSum = findViewById( R.id.tvSum );
        tvResult = findViewById( R.id.tvResult);
        millisUntilFinished = 30100;
        points = 0;
        numberOfQuestions = 0;
        random = new Random();
        btnIds = new int[]{R.id.btn0,R.id.btn1,R.id.btn2,R.id.btn3};
        correctAnswerPosition=0;
        incorrectAnswers = new ArrayList<>();
        startGame();
    }

    private  void startGame() {
        tvTimer.setText( ""+(millisUntilFinished/1000) + "s");
        tvPoints.setText( ""+points +"/" +numberOfQuestions );
        generateQuestion();

        countDownTimer = new CountDownTimer(millisUntilFinished,1000 ) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvTimer.setText("" + (millisUntilFinished / 1000) + "s");
            }

            @Override
            public void onFinish() {
             btn0.setClickable(false);
             btn1.setClickable(false);
             btn2.setClickable(false);
             btn3.setClickable(false);
                Intent intent = new Intent( StartGame.this,GameOver.class);
                intent.putExtra( "points",points);
                startActivity(intent );
                finish();
            }
        }.start();


    }

    private void generateQuestion() {
        numberOfQuestions++;
        op1 = random.nextInt(10);
        op2 = random.nextInt(10);
        sum = op1+op2;
        tvSum.setText(op1 + "+" + op2 + "=" );
        correctAnswerPosition = random.nextInt(4);
        ((Button) findViewById( btnIds[correctAnswerPosition])).setText(""+sum);
        while(true){
            if(incorrectAnswers.size() > 3 )
                break;

            op1 = random.nextInt(10);
            op2 = random.nextInt(10);
            sumOthers = op1 + op2;
            if(sumOthers == sum)
                continue;
            incorrectAnswers.add(sumOthers);
        }


    for(int i=0; i<3; i++) {
       if(i== correctAnswerPosition)
           continue;
        ((Button) findViewById( btnIds[i])).setText(""+ incorrectAnswers.get(i));

    }


     incorrectAnswers.clear();

    }

    public  void chooseAnswer(View view) {

        int answer = Integer.parseInt(((Button) view).getText().toString());

        if (answer == sum ) {
            points++;
            tvResult.setText( "Correct" );
        }else{
            tvResult.setText( "Wrong" );

        }

       tvPoints.setText( points + "/" + numberOfQuestions );
        generateQuestion();
    }
}
